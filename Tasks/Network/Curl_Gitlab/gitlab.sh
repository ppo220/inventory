#!/bin/bash

Token="cJhxSt2uTZQwvYHUrHS-"


func_help () {
echo "
-P Create new project
-B Create new branch
-D Delete branch
"
}


func_usage () {

echo "Usage: $0 [ -P [Name of the new project] | -B [Name of the new branch] | -D [Branch name to delete] | -h Help]"
exit 1

}


func_newproject () {

curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
-d '{"name":"'$1'"}' https://gitlab.com/api/v4/projects | jq

}


func_newbranch () {

curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
-d '{"branch":"'$1'", "ref":"master"}' \
https://gitlab.com/api/v4/projects/22401107/repository/branches | jq

}

func_deletebranch () {

curl -s -X DELETE -H "PRIVATE-TOKEN:$Token" \
https://gitlab.com/api/v4/projects/22401107/repository/branches/$1 | jq

}


if [ "$#" -eq 0 ]; then
        echo "Aborted, no parameters"
        func_usage
    elif [ "$#" -gt 2 ]; then
        echo "Aborted, wrong number of parameters: $#"
        func_usage
fi


case $1 in

    -h) func_help
    ;;
    -P) 
        [[ "$2" ]] && ProjectName="$2" || func_usage
        
        func_newproject "$ProjectName"
        
    ;;
    -B)
        [[ "$2" ]] && BranchName="$2" || func_usage
        
        func_newbranch "$BranchName"
    ;;
    -D)
        [[ "$2" ]] && BranchName="$2" || func_usage
        
        func_deletebranch "$BranchName"
    ;;
    *) func_usage
    ;;
    
esac
