---
**Исследование HTTP протокола**
1. Найти в интернете 8 различных status code HTTP.
В запросе и ответе должно содержаться не менее 5 header’s атрибутов.
```
GET /ListAccounts?authuser=1&pid=25&gpsia=1&source=ogb&mo=1&mn=1&hl=ru&origin=https%3A%2F%2Fdrive.google.com&ts=216 HTTP/2
Host: accounts.google.com
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate, br
Referer: https://drive.google.com/
Connection: keep-alive
Cookie: __Host-GAPS=1:e3UBQup2GeEcLbONtg4ZIp_CsLjp_j_SX3jFOwIBRhUikQ61m7cZLr49EcI-TZ5yEzCn9U5uXH3Fr_2AvVgcgasVGKjPdDULM5KgHp3JNISecw:U6Wt6Con_d0NPN-g; NID=204=AURF4bLLH8-3i9khRpsOR9TEPnQOxFWB2QVWnF3IJJEReJ4V56Evtil8w-ZMGGBKVaVpUnQ21PaVKrWHxM0UYVBBverCUw9pDRiZ2d3CUzX4x1aVtkyCT-1pqO3A_gnrqcREvGqRUHg4SWS8PkJvKlWgSP9QbT1nQax-S3T0dh-cxLmfzN2iUrKUocrDxFVW2NVU1Rfd3sFDX2JTV5UmZak6QvshD6CJ3fI4zPtMCuhxNoKMM3bCu9kM9tqTUTEYuhpfJJFG9E1R-y75cJEYMkdto-3yOlkV11qCMPjEMsEVJZ5GWPaYu9SSzsHbQ_smJmTeWhYPT20N8_IxX9UciVp4W50KvwAmjh-ttedD4AK_AennxEjBR8za4j4RxZtkMiN0f0uQTr1nRXJg46Kd2yrU; SMSV=ADHTe-DPJr5chMosjUq_TdUlMry6pqfkbSwKkXqVYQr3DmKl4n8DR2qn6Bd_EUUfZ7NXPfoDPYBORGE62_TX9aPeL9MdUztVFFoANM4JJ8cZxr3Z30GkoiH_K34E6lzu1hb8ikni84nZjQ7oUOa3whNeUb2sRilJAQ; SID=3wfODP-_7CXOuF-1-Ce82MtzZD1Ki_J-jcPfq_w6LTqjdN1AlFnANZIKtEn4sxjU4PsrCA.; __Secure-3PSID=3wfODP-_7CXOuF-1-Ce82MtzZD1Ki_J-jcPfq_w6LTqjdN1A4FuRenaEQLw9NauckefMSw.; LSID=CPanel|o.calendar.google.com|o.console.cloud.google.com|o.enterprise.google.com|o.gds.google.com|o.groups.google.com|o.mail.google.com|o.myaccount.google.com|o.takeout.google.com|s.UA|s.blogger|s.youtube:3wfODFFiC8ceGa-_gD3l-9oRQB47oB5YLg4Hgp9mA-B8UWrCbp-x1mtjqzNaB92nAkf5YQ.; __Host-3PLSID=CPanel|o.calendar.google.com|o.console.cloud.google.com|o.enterprise.google.com|o.gds.google.com|o.groups.google.com|o.mail.google.com|o.myaccount.google.com|o.takeout.google.com|s.UA|s.blogger|s.youtube:3wfODFFiC8ceGa-_gD3l-9oRQB47oB5YLg4Hgp9mA-B8UWrCgsNOQz4a6GmaFgW3fnonFA.; HSID=ArbSE9J3SqZcRqX3O; SSID=AhZ0ax8BB0lk3gEJ3; APISID=sGjC2lzp3kClolfu/A3myOwfkP6Fmn8Bxo; SAPISID=szrYbrCSjigZx-cz/A9oSf9Q0E2K0ctE6K; __Secure-3PAPISID=szrYbrCSjigZx-cz/A9oSf9Q0E2K0ctE6K; ACCOUNT_CHOOSER=AFx_qI5Y6NC5IajHhu0XAxeKM3TWVSl2oqCiYKVdb4akx72buWJdMs5MlnXd7lKKNNJBCS2ep2LSENum80c0eplwBy75IB_Cw0V7oEN9d7UJs26NcnjKBVkLDbA-Q-l6tLAIx1E1O70hpAu3yLtsbXDh1svfDbAzkFBOnDPbU9ZV_8NU7Bfabz8SeIR1k8YsF-3YzS_9yElHsM5sEbbo-0jYdtExZD1sIWSjxKQvw22lnDzRTVZhF5X7GlkfHbZDinAHpyx-9V1VTnJqXi35xJxRP9lHwGHF1G9qBCTthwLRaUKAHv4yX89PjOnzkod24e67R1-LsGL-7lrYgC39-R_YJ5EvLeX-Cg; SIDCC=AJi4QfEGOcL6y6DIgho4EHWWxyoC3MKlq60w_AAs_kZcs0SiOZBrHwrguSgMVlKbRBaFbIkBoV4; 1P_JAR=2020-11-30-13; __Secure-3PSIDCC=AJi4QfHTpNkkz_jFlPhXLStvJDAZc93sDe3b5dcMUGbjnumz5idtzudo2EIm05kvXM-ChLr1Okw; S=billing-ui-v3=lb6OB2HgVM0EpQdSporRJQbk9adOD14r:billing-ui-v3-efe=lb6OB2HgVM0EpQdSporRJQbk9adOD14r; user_id=102486966261757083101
Upgrade-Insecure-Requests: 1
Pragma: no-cache
Cache-Control: no-cache
TE: Trailers
```
2. Описать назначение всех атрибутов в client request and server response.
```
**Host** - Domain name of server.
**User-Agent** - User agent string with details about client side.
**Accept** - Media types acceptable by client.
**Accept-Language** - Languages acceptable by client.
**Accept-Encoding** - Acceptable encodings.
**Referer** - Address of page from where link to the current page was followed.
**Connection** - Options to control connection.
**Cookie** - Data sent by server to client
**Cache-Control** - Whether save data on client or not


```
3. Найти еще 7 различных status code.
Выполнять только после выполнения задания 1.

4. Произвести фильтрацию трафика протокола HTTP с помощью tcpdump.
Написать два фильтра:
a. фильтровать по методам протокола HTTP.
b. фильтровать по методу и header’s атрибуту в response протокола HTTP
c. фильтровать по методу и header’s атрибуту в request протокола HTTP

5. Используя Fiddler выполнить пункт 4.

6. Используя Fiddler попробовать вскрыть HTTPs.



