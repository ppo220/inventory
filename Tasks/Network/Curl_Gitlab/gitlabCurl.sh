#!/bin/bash

Token="cJhxSt2uTZQwvYHUrHS-"

ProjectName="CurlRepo"

# Create project
curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
-d '{"name":"'$ProjectName'"}' https://gitlab.com/api/v4/projects | jq

# Delete project by id
GetProjectId=$(curl -s --header "PRIVATE-TOKEN:$Token" https://gitlab.com/api/v4/users/ppo220/projects \
| jq '.[] | select (.name | contains("'$ProjectName'")) .id')

curl -s -X DELETE -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
https://gitlab.com/api/v4/projects/$GetProjectId | jq

# Get branches
curl -s --header "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/users/ppo220/projects" \
| jq '.[] | select (.name | contains("inventory")) .id'
22401107

curl -s "https://gitlab.com/api/v4/projects/22401107/repository/branches" | jq -r '.[].name'
develop
first
images
images2
master
second
styles
styles2

# Get merged branches
curl -s "https://gitlab.com/api/v4/projects/22401107/repository/branches" \
| jq '.[] | select (.merged | tostring | contains("true")) .name'
"develop"
"first"
"second"

curl -s "https://gitlab.com/api/v4/projects/22401107/repository/branches" \
| jq '.[] | select (.merged == true) | .name'
"develop"
"first"
"second"

# Get tags
curl -s "https://gitlab.com/api/v4/projects/22401107/repository/tags" \
| jq -c '.[] | {name,message}'
{"name":"version1.1","message":"Just a tag"}

# Create and assign an issue

curl -s -X POST -H "PRIVATE-TOKEN:$Token" \
"https://gitlab.com/api/v4/projects/22401107/issues?assignee_ids=7623326&title=The%20first%20issue&description=Just%20a%20description" | jq

curl -s -H "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/issues" \
| jq '.[0].id, .[0].title, .[0].assignee.name'
75839915
"The first issue"
"Pavlo Pocheptsov"

# Create merge request

curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
-d '{"source_branch": "styles", "target_branch": "styles2", "title": "Merge styles"}' \
https://gitlab.com/api/v4/projects/22401107/merge_requests | jq        
{
  "id": 81230404,
  "iid": 1,
  "project_id": 22401107,
  "title": "Merge styles",

# Create merge request with remove branches

curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
-d '{"source_branch": "images", "target_branch": "images2", "title": "Merge images and remove source branch", "remove_source_branch ":"true"}' \
https://gitlab.com/api/v4/projects/22401107/merge_requests | jq 
{
  "id": 81237272,
  "iid": 3,
  "project_id": 22401107,
  "title": "Merge images and remove source branch",

# Set tag on commit after merge

curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" \
> -d '{"tag_name":"v%201.2%20Merged%20Images", "ref":"bf04d120"}' \
> https://gitlab.com/api/v4/projects/22401107/repository/tags | jq
{
  "name": "v%201.2%20Merged%20Images",
  "message": "",
  "target": "bf04d1204f955c3f69c6b7b4017171a1d48c3b96",

# List of all users

curl -s -H "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/users" | jq '.'






















########List of projects#########

curl -s --header "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/users/ppo220/projects" | \
jq '.[] | {id,name}'
{
  "id": 22560658,
  "name": "gitrepo"
}
{
  "id": 22401107,
  "name": "inventory"
}

##########List of groups#########

curl -s --header "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/groups" | \
jq '.[]  | {id,name,path}'
{                                                                                           
  "id": 10345301,                                                                           
  "name": "Web",                                                                            
  "path": "web159"                                                                          
}

########Create projects#############

curl -s -H "Content-Type: application/json" -H "PRIVATE-TOKEN:$Token" -d '{"name":"CurlRepo"}' \
https://gitlab.com/api/v4/projects | jq                                                    
{                                                                                           
  "id": 22992643,
  "description": null,
  "name": "CurlRepo",
  "name_with_namespace": "Pavlo Pocheptsov / CurlRepo",
  "path": "curlrepo",
  "path_with_namespace": "ppo220/curlrepo",
  "created_at": "2020-12-09T08:26:07.867Z",

###########Delete project##############


{
  "message": "202 Accepted"
}

#######Get just values###############

curl -s --header "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/users/ppo220/projects" | \
jq '.[].id'                                
22994611
22560658
22401107

######Filter by keyvalue#########

curl -s --header "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/users/ppo220/projects" | \
jq '.[] | select (.name | contains("CurlRepo"))' 
{
  "id": 22994611,
  "description": null,
  "name": "CurlRepo",
  
#####Get specific value of key with filer###########

curl -s --header "PRIVATE-TOKEN:$Token" "https://gitlab.com/api/v4/users/ppo220/projects" | \
jq '.[] | select (.name | contains("CurlRepo")) .id'
22994611

