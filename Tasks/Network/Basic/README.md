**Анализ сетевых интерфейсов**
1. Исследовать сетевые настройки на вашем компьютере. Выполнить проверку
относительно всех доступных сетевых интерфейсов в системе. (ipconfig / ifconfig / ip)
```console
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether a4:5d:36:c9:f1:02 brd ff:ff:ff:ff:ff:ff
    inet 192.168.5.4/24 brd 192.168.5.255 scope global noprefixroute enp4s0
       valid_lft forever preferred_lft forever
    inet6 fe80::a65d:36ff:fec9:f102/64 scope link 
       valid_lft forever preferred_lft forever
3: wlo1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 3c:77:e6:17:a3:c1 brd ff:ff:ff:ff:ff:ff
    inet 10.10.100.12/24 brd 10.10.100.255 scope global noprefixroute wlo1
       valid_lft forever preferred_lft forever
    inet6 fe80::7da1:2857:13b6:428b/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:df:8d:12:e7 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever

```
2. Проверить качество связи (на домены ukr.net, ya.ru, 8.8.8.8), объяснить показатели. (ping)
```console
$ sudo ping -c 2 -s 1573 -M want ukr.net        
PING ukr.net (212.42.76.252) 1573(1601) bytes of data.
1581 bytes from srv252.fwdcdn.com (212.42.76.252): icmp_seq=1 ttl=60 time=38.9 ms
1581 bytes from srv252.fwdcdn.com (212.42.76.252): icmp_seq=2 ttl=60 time=38.9 ms
```
3. Проверить качество связи (на host студента), максимально нагрузить host. (ping, )
```
ping -f host
```
4. Изучение MTU:
a. получить значения MTU локальных интерфейсов;
```console
$ ip a | grep mtu
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
3: wlo1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
```
b. изменить значение MTU локальных интерфейсов. Определить допустимые
значения MTU. Как это отразится на канале связи?
```console
$ sudo ifconfig enp4s0 mtu 1200
$ ip a | grep enp | grep mtu
2: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1200 qdisc fq_codel state UP group default qlen 1000

С уменьшением MTU эффективность передачи данных может упасть.

$ sudo ifconfig enp4s0 mtu 1900
$ ip a | grep enp | grep mtu
2: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1900 qdisc fq_codel state UP group default qlen 1000
$ sudo ping -M do -s 1900 ukr.net
PING ukr.net (212.42.76.252) 1900(1928) bytes of data.
ping: local error: Message too long, mtu=1900

Если MTU на исходной машине выше чем MTU на каком-то хосте через который в дальнейшем идет траффик
и при этом пакетам запрещена фрагментация, то такой трафик не будет работать.

Допустимый MTU должен быть равен или меньше чем минимальный MTU,
установленный на пути траффика к хосту назначения.
```
c. Включите режим Jumbo Frame. Промоделировать преимущества и недостатки.
```console
$ sudo ip link set enp4s0 mtu 9000
$ ip link show | grep enp | grep mtu
2: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc fq_codel state UP mode DEFAULT group default qlen 1000

Если устройства в сети поддерживают этот режим, то эффективность передачи данных в сети может возрости.
Если устройства не поддерживают, то хост с активированным джумбоФреймом будет вынужден подстраиваться
под меньшие размеры MTU других хостов, то есть эффективность его работы может упасть.
```
d. Объединиться в команды по 3 студента. Два члена команды на своих VM изменяют
MTU и не сообщают его третьему участнику. Третий член команды должен
вычислить MTU канала связи. (Описать процесс вычисления). Все члены команды
должны написать свой скрипт для поиска MTU и выполнить поиск MTU.
```console
Алгоритм вычисления такой: запретить фрагментацию пинг пакетов и поочередно задавая больший/меньший
размер пакетов при пинге найти такое максимальное значение пакета, при котором пинг будет успешным.

MTU_NOMINAL=1500

MTU_MAX=9000
MTU_MIN=576

MTU=$MTU_MAX

ip link set enp4s0 mtu $MTU_MAX

while [ $MTU -ge $MTU_MIN ]; do
     
    if ping -c 1 -q -M do -s $MTU dest_host > /dev/null ; then
       
       echo "MTU on destination host is $MTU."
       
       exit 0 #break
       
       else
       
       ((MTU--))
       
    fi
     
done

echo "MTU on dest host isn't valid."

sudo ip link set enp4s0 mtu $MTU_NOMINAL

```

e. Измените длину очереди передачи и промоделируйте ее работу после изменений.
Сделайте несколько изменений.
```console
$ ifconfig enp4s0 | grep txque
        ether a4:5d:36:c9:f1:02  txqueuelen 1000  (Ethernet)
$ ifconfig enp4s0 txqueuelen 10000
$ ifconfig enp4s0 | grep txque
        ether a4:5d:36:c9:f1:02  txqueuelen 10000  (Ethernet)
```

**Изучение MAC**
1. Найти все доступные MAC-адреса в вашей сети (хосты коллег, ресурсов).
```console
$ nmap -sn 192.168.5.0/24
$ arp -an | cut -d" " -f4
00:18:7d:0e:f5:34
00:25:90:7c:46:ac
..
```
2. Используйте команды arp и ip.
3. Реализовать систему автоматического обнаружения изменений в локальной
сети.
Делать описание процесса выполнения (Какие команды? Почему такая
последовательность команд?).
Делать скриншоты, на которых можно проследить ключевые моменты выполнения.

**Администрирование**
1. Выполнить статическую настройку интерфейса.
a. Установить временный статический IP-адрес.
```
ip addr add 192.168.1.200/24 dev enp4so valid_lft 60  # set valid time to 1 minute
```
b. Установить перманентный статический IP-адрес.
```
ip addr add 192.168.1.200/24 dev enp4so
ifconfig enp4so 192.168.1.200/24
```
c. Установить статический IP-адрес с минимально допустимой маской для сети с
количеством компьютеров 2^(<последнее число вашего ID-пропуска>).
```
Number of hosts=2^5=32
Netmask=255.255.255.224=/27
Choosen network is 192.168.1.0
IPs=192.168.1.1 - 192.168.1.30
```
d. Способы изменения MAC-адреса в операционных системах. Установить локально
администрируемый MAC-адрес.
```
ifconfig enp4so hw ether xx:xx:xx:xx:xx:xx
ip link set enp4so address xx:xx:xx:xx:xx:xx
```
e. Проверить выполненное с помощью команды ip and ipconfig (ifconfig).
2. Настроить адрес шлюза. В случае нескольких интерфейсов как работают шлюзы.
```
ip route add 192.168.100.0/24 via 192.168.1.250 dev enp4so
ip route add default via 192.168.1.254 dev enp4so

Шлюзом по-умолчанию будет шлюз с меньшей метрикой. Если он будет недоступен,
то трафик пойдет через другой шлюз.
```
3. Назначение маски на хосте и на роутере.
```
Сетевая маска нужна для определения адреса сети, адреса хоста, широковещательного адреса.
Если отправляемый пакет в адресе назначения содержит адрес не из текущей сети, то он учитывая метрику
отправляется по маршруту к этой сети (если такой маршрут есть в локальной таблице) или отправляется
на адрес шлюза по-умолчанию.
```

**Анализ трафика**
1. Выполнить в ОС Windows и Linux
a. Wireshark или Message Analyzer
(https://www.youtube.com/playlist?list=PLszrKxVJQz5Uwi90w9j4sQorZosTYgDO4)
b. tcpdump
2. Захватить трафик на своем хосте
a. Найти кадры Ethernet
i. Найти unicast кадр (определить чей)
```
$ sudo tcpdump -i enp4s0 -n -c 5 not broadcast and not multicast
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:28:17.166543 IP 51.178.91.234.80 > 192.168.5.4.35189: Flags [.], ack 2098343498, win 501, options [nop,nop,TS val 3310032450 ecr 1310530221], length 0
17:28:17.166607 IP 192.168.5.4.35189 > 51.178.91.234.80: Flags [.], ack 1, win 501, options [nop,nop,TS val 1310540461 ecr 3310004201], length 0
17:28:18.598895 IP 52.11.109.209.443 > 192.168.5.4.51396: Flags [P.], seq 2197537911:2197537942, ack 758770535, win 118, options [nop,nop,TS val 2127669044 ecr 1837433964], length 31
17:28:18.599228 IP 192.168.5.4.51396 > 52.11.109.209.443: Flags [P.], seq 1:36, ack 31, win 501, options [nop,nop,TS val 1837734176 ecr 2127669044], length 35
17:28:18.808730 IP 52.11.109.209.443 > 192.168.5.4.51396: Flags [.], ack 36, win 118, options [nop,nop,TS val 2127669254 ecr 1837734176], length 0
```
ii. Найти широковещательный кадр (определить чей)
```
$ sudo tcpdump -i enp4s0 -n -c 5 ether broadcast
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:33:18.685633 ARP, Request who-has 192.168.5.233 tell 192.168.5.220, length 46
17:33:19.685661 ARP, Request who-has 192.168.5.233 tell 192.168.5.220, length 46
17:33:19.812254 98:da:c4:6d:7c:33 > ff:ff:ff:ff:ff:ff, RRCP-0x25 reply
17:33:20.027344 ARP, Request who-has 192.168.5.48 tell 192.168.5.100, length 46
17:33:20.225789 IP 192.168.5.27.53342 > 192.168.5.255.1947: UDP, length 40
```
iii. Перехватить трафик, моделирующий работу протокола ARP.
```
$ sudo tcpdump -i enp4s0 -n -c 5 arp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:29:46.663264 ARP, Request who-has 169.254.169.254 tell 192.168.5.46, length 46
17:29:46.814097 ARP, Request who-has 192.168.5.233 tell 192.168.5.219, length 46
17:29:47.663296 ARP, Request who-has 169.254.169.254 tell 192.168.5.46, length 46
17:29:47.814052 ARP, Request who-has 192.168.5.233 tell 192.168.5.219, length 46
17:29:48.020138 ARP, Request who-has 192.168.5.233 tell 192.168.5.221, length 46
```
b. Найти пакеты IP
i. Определить чей он и какой вид трафика
ii. Найти входящий и исходящий ip пакет
iii. Найти пакеты, которые являются (unicast, broadcast, multicast)
```
$ sudo tcpdump -i enp4s0 -n -c 5 ip and not broadcast and not multicast
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:37:46.460686 IP 192.168.5.4.40346 > 172.217.20.163.443: Flags [P.], seq 3034754965:3034755004, ack 3613730210, win 501, options [nop,nop,TS val 1778518408 ecr 2314192538], length 39
17:37:46.483114 IP 172.217.20.163.443 > 192.168.5.4.40346: Flags [.], ack 39, win 276, options [nop,nop,TS val 2314250925 ecr 1778518408], length 0
17:37:46.483115 IP 172.217.20.163.443 > 192.168.5.4.40346: Flags [P.], seq 1:40, ack 39, win 276, options [nop,nop,TS val 2314250925 ecr 1778518408], length 39
17:37:46.483194 IP 192.168.5.4.40346 > 172.217.20.163.443: Flags [.], ack 40, win 501, options [nop,nop,TS val 1778518430 ecr 2314250925], length 0
17:37:46.940966 IP 173.194.73.189.443 > 192.168.5.4.53618: Flags [P.], seq 3614909663:3614909716, ack 4226230079, win 366, options [nop,nop,TS val 1452768157 ecr 489021594], length 53

$ sudo tcpdump -i enp4s0 -n -c 5 ip broadcast
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:36:15.017398 IP 192.168.5.221.55707 > 255.255.255.255.10001: UDP, length 160
17:36:15.368503 IP 192.168.5.220.56534 > 255.255.255.255.10001: UDP, length 160
17:36:20.390952 IP 192.168.5.218.59103 > 255.255.255.255.10001: UDP, length 162
17:36:20.543851 IP 192.168.5.219.50689 > 255.255.255.255.10001: UDP, length 160
17:36:25.022053 IP 192.168.5.221.41968 > 255.255.255.255.10001: UDP, length 160

$ sudo tcpdump -i enp4s0 -n -c 5 ip multicast
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:38:20.132107 IP 192.168.5.170.64389 > 239.255.255.250.1900: UDP, length 276
17:38:20.281254 IP 192.168.5.236 > 224.0.0.1: igmp query v3
17:38:20.342022 IP 192.168.5.170 > 224.0.0.22: igmp v3 report, 1 group record(s)
17:38:20.447502 IP 192.168.5.218.55576 > 255.255.255.255.10001: UDP, length 162
17:38:20.473161 IP 192.168.5.78 > 224.0.0.22: igmp v3 report, 1 group record(s)
```
c. Найти сегменты:
i. TCP которые подтверждают процесс установки соединения (handshaking)
```
$ sudo tcpdump -i enp4s0 -n -c 10 host 192.168.5.200
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:40:13.619669 IP 192.168.5.4.37278 > 192.168.5.200.22: Flags [S], seq 3099543728, win 64240, options [mss 1460,sackOK,TS val 4240301186 ecr 0,nop,wscale 7], length 0
17:40:13.628375 ARP, Request who-has 192.168.5.4 tell 192.168.5.200, length 46
17:40:13.628441 ARP, Reply 192.168.5.4 is-at a4:5d:36:c9:f1:02, length 28
17:40:13.628506 IP 192.168.5.200.22 > 192.168.5.4.37278: Flags [S.], seq 1242448947, ack 3099543729, win 5792, options [mss 1460,sackOK,TS val 2331411538 ecr 4240301186,nop,wscale 5], length 0
17:40:13.628605 IP 192.168.5.4.37278 > 192.168.5.200.22: Flags [.], ack 1, win 502, options [nop,nop,TS val 4240301195 ecr 2331411538], length 0
```
ii. TCP которые подтверждают процесс передачи данных соединения
(ESTABLISHED)
```
17:43:23.532025 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [P.], seq 42:1402, ack 22, win 502, options [nop,nop,TS val 298564461 ecr 1697605793], length 1360
17:43:23.550297 IP 192.168.5.245.22 > 192.168.5.4.36760: Flags [P.], seq 22:862, ack 1402, win 272, options [nop,nop,TS val 1697605812 ecr 298564461], length 840
17:43:23.550397 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [.], ack 862, win 501, options [nop,nop,TS val 298564480 ecr 1697605812], length 0
```
iii. TCP которые подтверждают общение клиента и сервера в состоянии
соединение установлено (ESTABLISHED), но без передачи данных.
```
17:43:31.883296 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [P.], seq 4194:4234, ack 3222, win 501, options [nop,nop,TS val 298572813 ecr 1697606228], length 40
17:43:31.885056 IP 192.168.5.245.22 > 192.168.5.4.36760: Flags [P.], seq 3222:3262, ack 4234, win 445, options [nop,nop,TS val 1697614147 ecr 298572813], length 40
17:43:31.885161 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [.], ack 3262, win 501, options [nop,nop,TS val 298572814 ecr 1697614147], length 0
17:43:31.963450 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [P.], seq 4234:4274, ack 3262, win 501, options [nop,nop,TS val 298572893 ecr 1697614147], length 40
17:43:31.964998 IP 192.168.5.245.22 > 192.168.5.4.36760: Flags [P.], seq 3262:3302, ack 4274, win 445, options [nop,nop,TS val 1697614227 ecr 298572893], length 40
```
iv. TCP которые подтверждают процесс завершения соединения
```
17:43:37.918217 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [F.], seq 4594, ack 4462, win 501, options [nop,nop,TS val 298578848 ecr 1697620179], length 0
17:43:37.918285 IP 192.168.5.245.22 > 192.168.5.4.36760: Flags [.], ack 4594, win 445, options [nop,nop,TS val 1697620180 ecr 298578847], length 0
17:43:37.919606 IP 192.168.5.245.22 > 192.168.5.4.36760: Flags [F.], seq 4462, ack 4595, win 445, options [nop,nop,TS val 1697620181 ecr 298578848], length 0
17:43:37.919678 IP 192.168.5.4.36760 > 192.168.5.245.22: Flags [.], ack 4463, win 501, options [nop,nop,TS val 298578849 ecr 1697620181], length 0
```
v. * TCP которые подтверждают прекращения передачи трафика в следствии,
переполнения буфера получателя (описать процесс проверки).

vi. * TCP которые подтверждают повторную передачу сегментов в связи их
потерей при передаче (описать процесс проверки).

d. Найти данные протоколы (настроить фильтр):

i. DNS (UDP/TCP дейтаграммы)
```
$ sudo tcpdump -i enp4s0 -n -c5 port 53
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:55:26.046166 IP 192.168.5.4.47960 > 192.168.5.254.53: 42414+ [1au] A? github.com. (39)
17:55:26.091263 IP 192.168.5.254.53 > 192.168.5.4.47960: 42414 1/8/11 A 140.82.121.3 (462)
17:55:26.105361 IP 192.168.5.4.35443 > 192.168.5.254.53: 31838+ [1au] A? yahoo.com. (38)
17:55:26.105550 IP 192.168.5.4.39771 > 192.168.5.254.53: 7224+ [1au] AAAA? yahoo.com. (38)
17:55:26.127214 IP 192.168.5.254.53 > 192.168.5.4.35443: 31838 6/5/10 A 74.6.231.20, A 98.137.11.163, A 98.137.11.164, A 74.6.231.21, A 74.6.143.25, A 74.6.143.26 (416)
```
ii. DHCP (UDP/TCP дейтаграммы)
```
$ sudo tcpdump -i enp4s0 -n port 67 or port 68
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:57:52.774225 IP 0.0.0.0.68 > 255.255.255.255.67: BOOTP/DHCP, Request from a4:5d:36:c9:f1:02, length 300
17:57:52.775966 IP 192.168.5.254.67 > 192.168.5.4.68: BOOTP/DHCP, Reply, length 312
17:57:54.432058 IP 192.168.5.28.68 > 255.255.255.255.67: BOOTP/DHCP, Request from e0:cb:4e:ba:69:9f, length 300
17:57:55.463126 IP 0.0.0.0.68 > 255.255.255.255.67: BOOTP/DHCP, Request from 50:5b:c2:6f:82:a1, length 322
```
iii. HTTP (TCP’s segments)
```
$ sudo tcpdump -i enp4s0 -n -A -s 1000 port 80
10:00:57.404515 IP 192.168.5.4.50388 > 192.168.5.251.80: Flags [P.], seq 1:407, ack 1, win 501, options [nop,nop,TS val 747774519 ecr 1023676260], length 406: HTTP: GET /scm HTTP/1.1
E...a.@.@.K............P.uOh.=L=...........
,."7=..dGET /scm HTTP/1.1
Host: scm.ektos.net
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Cookie: JSESSIONID=10v2vhplt8qlz9cxkgf28hmnt; theme_cookie=dark-theme
Upgrade-Insecure-Requests: 1


10:00:57.405160 IP 192.168.5.251.80 > 192.168.5.4.50388: Flags [P.], seq 1:112, ack 407, win 285, options [nop,nop,TS val 1023683099 ecr 747774519], length 111: HTTP: HTTP/1.1 302 Found
E...1.@.@.|..........P...=L=.uP............
=.*.,."7HTTP/1.1 302 Found
Location: http://scm.ektos.net/scm/
Content-Length: 0
Server: Jetty(7.6.21.v20160908)
```
iv. * HTTPS (TCP’s segments)
```
$ sudo tcpdump -i enp4s0 -n -A -s 1000 host 192.168.5.243 and port 443
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 1000 bytes
10:08:37.947187 IP 192.168.5.4.37264 > 192.168.5.243.443: Flags [S], seq 479745336, win 64240, options [mss 1460,sackOK,TS val 516282316 ecr 0,nop,wscale 7], length 0
E..<    Y@.@.................U8.........v.........
............
10:08:37.947561 IP 192.168.5.243.443 > 192.168.5.4.37264: Flags [S.], seq 180779214, ack 479745337, win 28960, options [mss 1460,sackOK,TS val 2508773411 ecr 516282316,nop,wscale 7], length 0
E..<..@.@..t............
.x...U9..q ]..........
...#........
10:08:37.947601 IP 192.168.5.4.37264 > 192.168.5.243.443: Flags [.], ack 1, win 502, options [nop,nop,TS val 516282317 ecr 2508773411], length 0
E..4    Z@.@.."..............U9
.x......n.....
.......#
10:08:37.951103 IP 192.168.5.4.37264 > 192.168.5.243.443: Flags [P.], seq 1:581, ack 1, win 502, options [nop,nop,TS val 516282320 ecr 2508773411], length 580
E..x    [@.@.................U9
.x............
.......#....?...;..f.3."..../..iY.'V;m'QrYqO&.&.?B. }.HU.VJ...S&..
....8.B:n.Z.i...d.$.......+./.....,.0.
.       ........./.5.
.............jenkins.ektos.net..........
.......................#..Y>H........m5.=.(!:..%p..wf..0......].X.Q..T....^..8..f.L.R...\.oZ.7.C..vf.r!..m...n..[.&...G3v.E.........@3..p...........Q(....&.rk..6.n......(.bo...j.....g.......v.~.|[.:..x.Y.G..E..Lp.!..q8$.....1D....a.Ia.......h2.http/1.1..........3.k.i... ...b..W../...._z...A.        ..g...im.
...A.L..By....l..j..9.....Q^.q.../;B..e...`.....S...@.Ku8B@?3.Yx...s..+.        ......................................-........@.
10:08:37.951394 IP 192.168.5.243.443 > 192.168.5.4.37264: Flags [.], ack 581, win 236, options [nop,nop,TS val 2508773415 ecr 516282320], length 0
E..4TD@.@.Z8............
.x...W}.....L.....
...'....
10:08:37.951601 IP 192.168.5.243.443 > 192.168.5.4.37264: Flags [P.], seq 1:157, ack 581, win 236, options [nop,nop,TS val 2508773415 ecr 516282320], length 156
E...TE@.@.Y.............
.x...W}....-G.....
...'........d...`..t.S.....r.9.-[......(.G....0.T[. }.HU.VJ...S&..
....8.B:n.Z.i...d./.............        .http/1.1..............(..=..p.....r..J......ac.7...3..M".h...'|
10:08:37.951625 IP 192.168.5.4.37264 > 192.168.5.243.443: Flags [.], ack 157, win 501, options [nop,nop,TS val 516282321 ecr 2508773415], length 0
E..4    \@.@.. ..............W}
.yk.....n.....
.......'
10:08:37.952084 IP 192.168.5.4.37264 > 192.168.5.243.443: Flags [P.], seq 581:632, ack 157, win 501, options [nop,nop,TS val 516282321 ecr 2508773415], length 51
E..g    ]@.@.................W}
.yk...........
.......'..........(..............._.G<.d......>.._so.2.xsA.
10:08:37.953586 IP 192.168.5.4.37264 > 192.168.5.243.443: Flags [P.], seq 632:1575, ack 157, win 501, options [nop,nop,TS val 516282323 ecr 2508773415], length 943
E...    ^@.@..o..............W.
.yk...........
.......'......................o\....,._.....I57.G.C<..j...gl.\eH............A....x..o..1G/`..j...^...O.RQ2^U)d.$5j...............Z....k...c.......B]..f.    2..c.{EY....-.[6=@~x...'D....(.7]..O..Y.......9;.`.6...j.Z!..5..9..
..vd{....{Mh.-.g..o .i...w..3Gh..UF...~..*o.3..f...\i.v[...x8.\...o#Zd%J..)..a..Gb..<
O2......{*.fn.D..J......M
..,.? C.h..2]ZC..0....k.._[...5.y.8..y1.!..>r.9vh...{mM.g.....K.1.....6BB..&[V. ....,=k.....3....`.y..7..*t..".....z..B..o.I..6.....U..&5..7...c.fD.P...^..[.......}........R...{.`;mo4.h..~...N.L..f!..L...L.."XV..........).. K._..7....8.....bP*....c...].2.U.\.NY9_.l..3......I.U.. ...f..B1-.:f.s].`i.....>..'...{("...,..kS.1...Z....fv...3{.......@...b.D..#...w..p.j..iW...]X...6.....1......@.(o_.]r..8.y.9.@k..V<.|........F..7......0s.      ....    S5....V.._...q:J.r....EW.1]...Hf.....W......s}.x.d.J....;...*....t.d.......b..V.b.q..(.?..\...6bL'..6...Y.p..sb.....v..x....J....0.I.^.l.....W?2&fu..V.L.r6....Mx;...".y.
10:08:37.953872 IP 192.168.5.243.443 > 192.168.5.4.37264: Flags [.], ack 1575, win 251, options [nop,nop,TS val 2508773417 ecr 516282321], length 0
E..4TF@.@.Z6............
.yk..[_...........
...)....
```
v. * TLS (расшифровать зашифрованный трафик)
```
$ sudo ssldump -k server.key -dnq host 192.168.5.245
New TCP connection #1: 192.168.5.4(57442) <-> 192.168.5.245(443)
1 1  0.0040 (0.0040)  C>S  Handshake      ClientHello
1 2  0.0044 (0.0004)  S>C  Handshake      ServerHello
1 3  0.0044 (0.0000)  S>C  ChangeCipherSpec
1 4  0.0044 (0.0000)  S>C  Handshake
1 5  0.0047 (0.0003)  C>S  ChangeCipherSpec
1 6  0.0047 (0.0000)  C>S  Handshake
1 7  0.0058 (0.0010)  C>S  application_data
```
3. Найти пакеты, которые подтверждают выполнение фрагментации IP на Ethernet.
(определить какие размеры пакетов пришли получателю и какое количество)
```
$ sudo ping -c 1 -s 2000 -M want ukr.net 
PING ukr.net (212.42.76.252) 2000(2028) bytes of data.
2008 bytes from srv252.fwdcdn.com (212.42.76.252): icmp_seq=1 ttl=60 time=39.0 ms

$ sudo tcpdump -i enp4s0 -nv icmp
tcpdump: listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
10:23:29.464294 IP (tos 0x0, ttl 64, id 57826, offset 0, flags [+], proto ICMP (1), length 1500)
    192.168.5.4 > 212.42.76.252: ICMP echo request, id 21880, seq 1, length 1480
10:23:29.464311 IP (tos 0x0, ttl 64, id 57826, offset 1480, flags [none], proto ICMP (1), length 548)
    192.168.5.4 > 212.42.76.252: ip-proto-1
10:23:29.503338 IP (tos 0x0, ttl 60, id 1226, offset 1480, flags [none], proto ICMP (1), length 548)
    212.42.76.252 > 192.168.5.4: ip-proto-1
10:23:29.503341 IP (tos 0x0, ttl 60, id 1226, offset 0, flags [+], proto ICMP (1), length 1500)
    212.42.76.252 > 192.168.5.4: ICMP echo reply, id 21880, seq 1, length 1480
```
4. Выполнить поиск логинов и паролей в трафике HTTP и FTP.
```
$ sudo tcpdump -i enp4s0 -n -l -A -s 0 port 80 or port 21 | grep -E "pass=|pwd=|log=|login=|user=|username=|pw=|passw=|passwd=|password=|pass:|user:|username:|password:|login:|pass |user |Authorization:"
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp4s0, link-type EN10MB (Ethernet), capture size 262144 bytes
Authorization: Basic bmVydmU6MTEx
```
5. Выполнить тестирование пропускной способности сети по протоколам TCP, UDP и SCTP* с
использованием Iperf3. (https://ru.wikipedia.org/wiki/Iperf). Public Iperf3 servers
(https://iperf.cc/)
```
$ sudo iperf3 -c iperf.scottlinux.com
Connecting to host iperf.scottlinux.com, port 5201
[  4] local 192.168.5.4 port 46882 connected to 45.33.39.39 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec  1.35 MBytes  11.3 Mbits/sec    0    221 KBytes       
[  4]   1.00-2.00   sec  5.57 MBytes  46.7 Mbits/sec  109   2.68 MBytes       
[  4]   2.00-3.00   sec  6.95 MBytes  58.3 Mbits/sec  868   1.57 MBytes       
[  4]   3.00-4.00   sec  6.18 MBytes  51.8 Mbits/sec   80   1.17 MBytes       
[  4]   4.00-5.00   sec  5.94 MBytes  49.8 Mbits/sec    0   1.23 MBytes       
[  4]   5.00-6.00   sec  6.25 MBytes  52.4 Mbits/sec    0   1.28 MBytes       
[  4]   6.00-7.00   sec  6.58 MBytes  55.2 Mbits/sec    0   1.31 MBytes       
[  4]   7.00-8.00   sec  6.77 MBytes  56.8 Mbits/sec    0   1.33 MBytes       
[  4]   8.00-9.00   sec  6.77 MBytes  56.8 Mbits/sec    0   1.34 MBytes       
[  4]   9.00-10.00  sec  6.83 MBytes  57.3 Mbits/sec    0   1.35 MBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  59.2 MBytes  49.6 Mbits/sec  1057             sender
[  4]   0.00-10.00  sec  56.8 MBytes  47.7 Mbits/sec                  receiver

iperf Done.

$ sudo iperf3 -c iperf.scottlinux.com -u
Connecting to host iperf.scottlinux.com, port 5201
[  4] local 192.168.5.4 port 50021 connected to 45.33.39.39 port 5201
[ ID] Interval           Transfer     Bandwidth       Total Datagrams
[  4]   0.00-1.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   1.00-2.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   2.00-3.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   3.00-4.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   4.00-5.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   5.00-6.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   6.00-7.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   7.00-8.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   8.00-9.00   sec   128 KBytes  1.05 Mbits/sec  16  
[  4]   9.00-10.00  sec   128 KBytes  1.05 Mbits/sec  16  
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-10.00  sec  1.25 MBytes  1.05 Mbits/sec  3985902.230 ms  2/160 (1.2%)  
[  4] Sent 160 datagrams

iperf Done.
```

**Диагностика хостов**
1. Выполнить мониторинг сетевой активности локальной системы (команда netstat, ss, iptraf, nc)
a. Выявить активные соединения
```
$ ss -tunp
State       Recv-Q        Send-Q                       Local Address:Port                          Peer Address:Port                                                          
ESTAB       0             0                              192.168.5.4:47980                        172.217.16.14:443          users:(("firefox",pid=2712,fd=264))              
ESTAB       0             0                              192.168.5.4:60160                         140.82.121.3:443          users:(("firefox",pid=2712,fd=221))              
ESTAB       0             0                              192.168.5.4:36364                       149.154.167.51:443          users:(("Telegram",pid=22728,fd=34))             
ESTAB       0             0                              192.168.5.4:41634                        68.232.34.200:443          users:(("skypeforlinux",pid=19521,fd=31))        
ESTAB       0             0                              192.168.5.4:50496                         13.107.6.163:443          users:(("skypeforlinux",pid=19521,fd=34))        
ESTAB       0             0                              192.168.5.4:57098                         13.107.3.254:443          users:(("skypeforlinux",pid=19521,fd=33))        
ESTAB       0             0                              192.168.5.4:54924                        216.58.215.99:443          users:(("firefox",pid=2712,fd=185))              
ESTAB       0             0                              192.168.5.4:37510                       104.18.136.190:80           users:(("http.so",pid=28156,fd=10))              
ESTAB       0             0                              192.168.5.4:46288                        52.159.49.199:443          users:(("skypeforlinux",pid=19521,fd=28))        
ESTAB       0             0                              192.168.5.4:40462                       35.167.145.222:443          users:(("firefox",pid=2712,fd=199))              
ESTAB       0             0                              192.168.5.4:38985                         13.69.158.96:443          users:(("skypeforlinux",pid=19544,fd=75)) 
```
b. Проанализировать открытые порты (UDP, TCP). Дать их классификацию
```
$ ss -4tunpl
Netid   State     Recv-Q    Send-Q        Local Address:Port          Peer Address:Port                                           
udp     UNCONN    0         0             127.0.0.53%lo:53                 0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:68                 0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:68                 0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:111                0.0.0.0:*                                              
udp     UNCONN    0         0              10.10.100.12:123                0.0.0.0:*                                              
udp     UNCONN    0         0               192.168.5.4:123                0.0.0.0:*                                              
udp     UNCONN    0         0                 127.0.0.1:123                0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:123                0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:4500               0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:500                0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:631                0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:763                0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:5353               0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:1701               0.0.0.0:*                                              
udp     UNCONN    0         0                   0.0.0.0:51480              0.0.0.0:*                                              
tcp     LISTEN    0         128           127.0.0.53%lo:53                 0.0.0.0:*                                              
tcp     LISTEN    0         128                 0.0.0.0:22                 0.0.0.0:*                                              
tcp     LISTEN    0         5                 127.0.0.1:631                0.0.0.0:*                                              
tcp     LISTEN    0         50                127.0.0.1:45112              0.0.0.0:*        users:(("Viber",pid=19655,fd=247))    
tcp     LISTEN    0         1                   0.0.0.0:7070               0.0.0.0:*        users:(("anydesk",pid=5538,fd=32))    
tcp     LISTEN    0         50                127.0.0.1:30666              0.0.0.0:*        users:(("Viber",pid=19655,fd=215)) 
```
c. Объяснить в каком состоянии находятся сетевые соединение
```
Сетевые соединения находятся в состоянии LISTEN или UNCONN (для UDP) - то есть ждут
подключений на указанный адрес:порт.
После установления соединения они переходят в состояние ESTABLISHED.
```
d. Определить основные, запущенные сетевые службы (процессы). Какие их них
работают в режиме сервера
```
В режиме сервера работают службы, которые имеют открытые порты, то есть
идентифицируются состоянием LISTEN.
```
e. Объяснить в каком состоянии находится соединение

2. Выполнить проверку портов (netstat, ss, iptraf, nc):
a. на локальном хосте
b. на удаленном хосте
c. * Реализовать программный способ проверки портов. Можно использовать только (java, python, ruby).

