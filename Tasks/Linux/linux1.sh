#!/bin/bash
#set -x

pause_func () {

while :; do
   read -n 1
   [[ $? = 0 ]] && break
done

}


clear

task1-5 () {

clear
while :; do

cat << 'MENUITEM'

    1. Найти все системные группы и получить только их уникальные имена и id. Сохранить в файл.
    
       cut -d: -f1,3 /etc/group | tee file  >Executable<
       
    2. Найти все файлы и директории, который имеют права для доступа соответствующих user и group.
    
       find / -user username -group groupname
    
    3. Найти все скрипты в указанной директории и ее поддиректориях.
    
       find . -type f -name "*.sh"   >Executable<
    
    4. Выполнить поиск файлов скриптов из под определенного пользователя.
    
       sudo -u username find / -type f -name "*.sh"
    
    5. Выполнить рекурсивный поиск слов или фразы для определенного типа файлов.
    
       find . -type f -name "*.txt" -exec grep -l 123 {} \;   >Executable<
       
    b.  Back to Main Menu

MENUITEM
echo -n "Enter: "
read -n 1 NUMBER

  case $NUMBER in

    1) echo; cut -d: -f1,3 /etc/group | tee file | more
    ;;
    3) echo; find . -type f -name "*.sh"
    pause_func
    ;;
    5) echo; find . -type f -name "*.txt" -exec grep -l 123 {} \;
    pause_func
    ;;
   b) main_menu
  ;;
  esac

done

}

task6-10 () {

clear
while :; do

cat << 'MENUITEM'

    6. Найти дубликаты файлов в заданных каталогах.
       Вначале сравнивать по размеру, затем по варианту
       (выбрать хешь функцию: CRC32, MD5, SHA-1, sha224sum).
       Результат должен быть отсортирован по имени файла.
       
       comm -12 <(ls -1 dir1) <(ls -1 dir2) | sort   >Executable<
       
    7. Найти по имени файла и его пути все символьные ссылки на него.
    
       find -L . -samefile fileee   >Executable<
    
    8. Найти по имени файла и его пути все жесткие ссылки на него.
    
      find . -inum $(ls -i fileee | cut -d" " -f1)   >Executable<
    
    9. Имеется только inode файла найти все его имена.
    
       find / -inum inum_number
    
    10. Имеется только inode файла найти все его имена.
        Учтите, что может быть примонтированно несколько разделов.
    
    b.  Back to Main Menu

MENUITEM
echo -n "Enter: "
read -n 1 NUMBER

  case $NUMBER in

    6) echo; comm -12 <(ls -1 dir1) <(ls -1 dir2) | sort
    pause_func
    ;;
    7) echo; find -L . -samefile fileee
    pause_func
    ;;
    8) echo; find . -inum $(ls -i fileee | cut -d" " -f1)
    pause_func
    ;;
    q) echo; exit 0
    pause_func
  ;;
   b) main_menu
  ;;
  esac

done

}

task11-15 () {

clear
while :; do

cat << 'MENUITEM'

    11. Корректно удалить файл с учетом возможности существования символьных или жестких ссылок.
    
        find / -inum $(ls -i filename | cut -d" " -f1) -delete
    
    12. Рекурсивно изменить права доступа к файлам (задана маска файла) в заданной директории.
    
        chmod -R xxx /target_dir
    
    13. Сравнить рекурсивно две директории и отобразить только отличающиеся файлы.
       (вывести до 2 строки и после 3 строки относительно строки в которой найдено отличие). 
       
       diff dir1 dir2   >Executable<
       
    14. Получить MAC-адреса сетевых интерфейсов
    
        ip link   >Executable<
    
    15. Вывести список пользователей, авторизованных в системе на текущий момент 
    
        who   >Executable<
    
    b.  Back to Main Menu

MENUITEM
echo -n "Enter: "
read NUMBER

  case $NUMBER in
  13) echo; diff dir1 dir2
  pause_func
  ;;
  14) echo; ip link
  pause_func
  ;;
  15) echo; who
    pause_func
  ;;
   b) main_menu
  ;;
  esac

done

}

task16-20 () {
clear
while :; do

cat << 'MENUITEM'

    16. Вывести список активных сетевых соединений в виде таблицы:
        тип состояния соединения и их количество 
        
        netstat -tn   >Executable<
        
    17. Переназначить существующую символьную ссылку. 
    
        ln -sf /path ./newlink
        
    18. Имется список фалов с относительным путем и путем к каталогу в котором должна
        храниться символьная ссылка на файл. Создать символьные ссылки на эти файлы 
        
        for f in *; do ln -s /path/$f $f; done
        
    19. Скопировать директорию с учетом, что в ней существуют как прямые так относительные
        символьные ссылки на файлы и директории. Предполагается, что копирование выполняется
        for backup on a removable storage. (сделать в двух вариантах, без rsync и с rsync) 
        
        cp -RLpf source dest
        rsync -az source dest
        
    20. Скопировать директорию с учетом, что в ней существуют прямые символьные относительные символьные ссылки
    
        cp -L source dest
    
    b.  Back to Main Menu

MENUITEM
echo -n "Enter: "
read NUMBER

  case $NUMBER in

  16) echo; netstat -tn
    pause_func
  ;;
   b) main_menu
  ;;
  esac

done
}

task21-25 () {
clear
while :; do

cat << 'MENUITEM'

    21. В директории проекта преобразовать все относительные ссылки в прямые 
    
    for link in $(find . -type l); do
      linkname=$(basename $link)
      filename=$(readlink $linkname)
      path=$(dirname $(readlink -f $filename))
      ln -sf $path/$filename $linkname
   done
    
    22. В директории проекта преобразовать все прямые ссылки в относительные для директории проекта.
    
   for link in $(find . -type l); do
      linkname=$(basename $link)
      filename=$(readlink $linkname)
      ln -sf $filename $linkname
   done
    
    23. В указанной директории найти все сломанные ссылки и удалить их 
        
        find . -type l -exec test ! -e {} \; -print -delete   >Executable<
        
    24. Распаковать из архива tar, gz, bz2, lz, lzma, xz, Z определенный каталог в указанное место. 
    25. Рекурсивно скопировать структуру каталогов из указанной директории. (без файлов)
        
        find . -type d -exec mkdir -p /destdir/{} \;   >Executable<
        
    b.  Back to Main Menu

MENUITEM
echo -n "Enter: "
read NUMBER

  case $NUMBER in
  23) echo; find . -type l -exec test ! -e {} \; -print -delete
  pause_func
  ;;
  25) echo; find . -type d -exec mkdir -p /destdir/{} \;
  pause_func
  ;;
   b) main_menu
  ;;
  esac

done

}

task26-30 () {
clear
while :; do

cat << 'MENUITEM'

    26. Вывести список всех пользователей системы (только имена) по алфавиту 
        
        cut -d: -f1 /etc/passwd | sort   >Executable<
        
    27. Вывести список всех системных пользователей системы отсортированных по id, в формате: login id 
        
        cut -d: -f1,3 /etc/passwd | sort -t: -k2 -h   >Executable<
        
    28. Вывести список всех пользователей системы (только имена) отсортированные по id 
    
        sort -t: -k3 -h /etc/passwd | cut -d: -f1   >Executable<
    
    29. Вывести всех пользователей которые не имеют право авторизовываться или
        не имеют право авторизовываться в системе. (две команды)
        
        grep -E "false|nologin" /etc/passwd | cut -d: -f1   >Executable<
        
    30. Вывести всех пользователей которые (имеют/не имеют) терминала (bash, sh, zsh and etc.) (две команды) 
    
        grep -E "bash|sh|zsh" /etc/passwd | cut -d: -f1   >Executable<
    
    b.  Back to Main Menu

MENUITEM
echo -n "Enter: "
read NUMBER

  case $NUMBER in
  26) echo; cut -d: -f1 /etc/passwd | sort
  pause_func
  ;;
  27) echo; cut -d: -f1,3 /etc/passwd | sort -t: -k2 -h
  pause_func
  ;;
  28) echo; sort -t: -k3 -h /etc/passwd | cut -d: -f1
  pause_func
  ;;
  29) echo; grep -E "false|nologin" /etc/passwd | cut -d: -f1
  pause_func
  ;;
  30) echo; grep -E "bash|sh|zsh" /etc/passwd | cut -d: -f1
  pause_func
  ;;
   b) main_menu
  ;;
  esac

done


}

task31-38 () {

clear

while :; do

  cat << 'MENUITEM'

    31. Со страницы из интернета закачать все ссылки, которые на странице.
        Закачивать параллельно. Использовать curl и wget. Дать рекомендации по использованию.
        
        curl -O url1 url2 ...
        
    32. Остановить процессы, которые работают больше 5 дней. Команду ps не использовать. 
    33. Имется дериктория, в которой, существуют папки и файлы (*.txt & *.jpeg).
        Файлы *.txt и *.jpeg однозначно связаны между собой по префиксу имени.
        Файлы могут находиться в различном месте данной директории.
        Нужно удалить все *.jpeg для которых не существует файла *.txt
        
   for jpegfile in $(find . -type f -name "*.jpeg"); do
       
       jpegfile=$(basename $jpegfile)
       jpegfilename=${jpegfile%.*}
       
       if ! find . -type f -name "$jpegfilename.txt" | grep -q . ; then
          rm -f $jpegfile
       fi
   done
        
        
    34. Find your IP address using the command line: 
    
        ip a | grep enp4s0 | tail -1   >Executable<
    
    35. Получить все ip-адресса из текстового файла 
    36. Найти все активные хосты в: 
       - заданной сети,  
       - списке IP (hosts-server.txt) 
       используя/не используя nMAP;
       
       nmap -sn 192.168.5.0/24 | tail -1
       Nmap done: 256 IP addresses (62 hosts up) scanned in 6.80 seconds
   
   count=0
   for host in {1..254}; do
     if ping -c 1 -q 192.168.5.$host > /dev/null; then
        ((count++))
     fi
   done
   echo "Number of host with answer to ping: $count"
       
       
    37. Используя результат таска 36. Получить ip поднятых хостов 
        
        nmap -n -sn 192.168.5.0/24 | grep report | sed 's/Nmap scan report for //g'   >Executable<
        
    38. Получить все поддоиены из SSL сертификата. 
        
        openssl x509 -in mail-google-com.pem -text -noout | grep DNS | tr 'DNS:' ' ' | xargs   >Executable<
        
    b.  Back to Main Menu
MENUITEM
echo -n "Enter: "
read NUMBER

  case $NUMBER in

    34) echo; ip a | grep enp4s0 | tail -1
    pause_func
    ;;
    37) echo; nmap -n -sn 192.168.5.0/24 | grep report | sed 's/Nmap scan report for //g'
    pause_func
    ;;
    38) echo; openssl x509 -in mail-google-com.pem -text -noout | grep DNS | tr 'DNS:' ' ' | xargs
    pause_func
    ;;
    b) main_menu
    ;;
  esac
done
}

main_menu () {
clear
while :; do
    cat << MENUITEM
  1) task1-5
  2) task6-10
  3) task11-15
  4) task16-20
  5) task21-25
  6) task26-30
  7) task31-38
  q) Quit
MENUITEM
echo -n "Enter: "
read -n 1 NUMBER
  case $NUMBER in
    1) task1-5
    ;;
    2) task6-10
    ;;
    3) task11-15
    ;;
    4) task16-20
    ;;
    5) task21-25
    ;;
    6) task26-30
    ;;
    7) task31-38
    ;;
    q) echo; exit 0
    ;;
    *) echo; echo "Please select 1, 2 ... or q for quit"
    ;;
  esac
done
}

main_menu 
